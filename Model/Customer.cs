namespace webapi.Model
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Profession { get; set; }
        public int Age { get; set; }

    }
}